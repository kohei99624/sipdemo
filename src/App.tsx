import React from 'react';
import HomePage from './components/Home/HomePage';
// import './App.css'
import { BrowserRouter as Router, Route, Routes,} from 'react-router-dom'
import Sipjs from './components/Sipjs';

const App: React.FC = () => {
  return (
    <div id="App">
      <Router>
        <Routes>
          <Route path="/" Component ={HomePage} />
            <Route path="/sipjs" Component ={Sipjs} /> 
        </Routes>
      </Router>
    </div>
  );
};

export default App;

import { FC, useEffect } from 'react';
import { SessionDescriptionHandler } from 'sip.js/lib/platform/web';
import { Inviter, Registerer, SessionState,Session, UserAgent, UserAgentOptions, Invitation } from 'sip.js';

const Sipjs: FC = () => {
  let userAgent:UserAgent;
  let inviter:Inviter;
  //-------------------------------------------------------------------------------------------------------------------
  // const server = "wss://kohei.frd.cloud:8089/ws";

  // const aor = "sip:1060@kohei.frd.cloud";

  // const authorizationUsername = '22345660';

  // const authorizationPassword = 'Qwer4321';

  // const destination = "sip:1062@kohei.frd.cloud";
//-------------------------------------------------------------------------------------------------------------------
  const server = "wss://call-devel.frd.cloud:8089/ws";

  const aor = "sip:mktest2@call-devel.frd.cloud";

  const authorizationUsername = 'mktest2';

  const authorizationPassword = 'mktest2';

  const destination = "sip:3150151@call-devel.frd.cloud";
//-------------------------------------------------------------------------------------------------------------------
  function getAudioElement(id: string): HTMLAudioElement {
    const el = document.getElementById(id);
    if (!(el instanceof HTMLAudioElement)) {
      throw new Error(`Element "${id}" not found or not an audio element.`);
    }
    return el;
  }

  const remoteStream = new MediaStream();

  function onInvite(invitation:Invitation) {
    invitation.stateChange.addListener((state: SessionState) => {
      console.log(`Session state changed to ${state}`);
      switch (state) {
        case SessionState.Initial:
          break;
        case SessionState.Establishing:
          break;
        case SessionState.Established:
          setupRemoteMedia(invitation);
          break;
        case SessionState.Terminating:
          // fall through
        case SessionState.Terminated:
          cleanupMedia();
          break;
        default:
          throw new Error("Unknown session state.");
      }
    });
  }

  function endCall(session:Session) {
    switch(session.state) {
      case SessionState.Initial:
      case SessionState.Established:
        session.bye();
        break;
      case SessionState.Terminating:
      case SessionState.Terminated:
        break;
    }
  }
 
  useEffect(()=>{
    const transportOptions = {
      server: server
    };
    const uri = UserAgent.makeURI(aor);
    const userAgentOptions: UserAgentOptions = {
      authorizationPassword: authorizationPassword,
      authorizationUsername: authorizationUsername,
      transportOptions,
      delegate: {
        onInvite
      },
      uri
    };
    userAgent = new UserAgent(userAgentOptions);
    
  },[])

  const RegistUser = () => {
    const registerer = new Registerer(userAgent);
    userAgent.start().then(() => {
      registerer.register();
    });
  }

  function setupRemoteMedia(session: Session) {
    const mediaElement = getAudioElement("remoteAudio")
    let sessionDescriptionHandler: SessionDescriptionHandler = session.sessionDescriptionHandler as SessionDescriptionHandler
    if (sessionDescriptionHandler.peerConnection) {
      sessionDescriptionHandler.peerConnection.getReceivers().forEach((receiver) => {
        if (receiver.track) {
          remoteStream.addTrack(receiver.track);
        }
      });
      mediaElement.srcObject = remoteStream;
      mediaElement.play().catch((error) => {
        console.error('Error playing media:', error);
      });
    } 
    else {
      console.error('Peer connection not available.');
    }
  }

  function cleanupMedia() {
    const mediaElement = getAudioElement("remoteAudio")
    mediaElement.srcObject = null;
    mediaElement.pause();
  }

  const MakeCall = () => {
      const target = UserAgent.makeURI(destination);
      if(target!=undefined) {
        inviter = new Inviter(userAgent, target);
        inviter.stateChange.addListener((state: SessionState) => {
          console.log(`Session state changed to ${state}`);
          switch (state) {
            case SessionState.Initial:
              break;
            case SessionState.Establishing:
              break;
            case SessionState.Established:
              setupRemoteMedia(inviter);
              break;
            case SessionState.Terminating:
            case SessionState.Terminated:
              cleanupMedia();
              break;
            default:
              throw new Error("Unknown session state.");
          }
        });
        inviter.invite();
      }
  }

  const Hangup = () => {
    endCall(inviter)
  }
  return (
    <div className="text-center mt-[90px]">
      	<h1 className="text-4xl font-bold my-2">SIP.js Browser phone Test</h1>
        <audio id="remoteAudio" controls className='max-w-[300px] m-auto'>
          <p>Your browser doesn't support HTML5 audio.</p>
        </audio>
        <div className='p-4 max-w-[300px] m-auto bg-gray-400 text-white my-[20px] cursor-pointer' onClick={()=>RegistUser()}>Register</div>
        <div className='p-4 max-w-[300px] m-auto bg-gray-400 text-white my-[20px] cursor-pointer' onClick={()=>MakeCall()}>Make Call</div>
        <div className='p-4 max-w-[300px] m-auto bg-gray-400 text-white my-[20px] cursor-pointer' onClick={()=>Hangup()}>Hang up</div>
    </div>
  );
};

export default Sipjs;

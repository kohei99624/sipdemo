import { FC } from 'react';
import { useNavigate } from 'react-router-dom';

const HomePage = () => {
	const navi = useNavigate()

  return (
    <div className="text-center mt-[90px]">
      	<h1 className="text-4xl font-bold my-2">SIP Browser Phones</h1>
			<div className="p-4 max-w-[300px] m-auto bg-gray-400 text-white my-[20px] cursor-pointer" onClick={()=>navi('/sipjs')}>SIPJS(2022)</div>
    </div>
  );
};

export default HomePage;

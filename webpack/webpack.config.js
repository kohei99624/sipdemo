import {merge} from 'webpack-merge'
import commonConfig from './webpack.common.js'
import devConfig from './webpack.dev.js'

const config = merge(commonConfig, devConfig);

export default config;
// const webpack = require('webpack');
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin'
const envConfig = {
  mode: 'development',
  devServer: {
    hot: true,
    open: true,
  },
  devtool: 'cheap-module-source-map',
  plugins: [
    // Specify development API URL
    // new webpack.DefinePlugin({
    //     "process.env": {
    //         NODE_ENV: JSON.stringify("development"),
    //     },
    // }),
    new ReactRefreshWebpackPlugin(),
  ],
};

export default envConfig;